/**
 * Created by teliov on 8/14/14.
 */
/*** javascript and jquery functions for foto4un site **/

/*
collection of functions
 */

/** front form validation **/

var csrftoken = $.cookie("csrftoken");

function validate(form_id){
    console.log(typeof(form_id));
    var obj = $(form_id)[0].value;

    var form_field;
    if (form_id.contains("username")){
        form_field = "username"
    }
    else if(form_id.contains("email")){
        form_field = "email"
    }
    $.ajax({
        url: "/check_credentials/",

        data:{
            field_name: form_field,
            field_value: obj
        },

        type: "POST",

        dataType: "json",

        headers:{
            "X-CSRFToken": csrftoken
        },

        cache: false,

        success: function(json){
            console.log(json)
        }


    })
}



/**
 more specific functinos
 **/

function handleOnChange(object){
    object.onchange = validate("#"+object.id);
}
var email = $("#id_email")[0];
email.onfocus = handleOnChange(email);
