/****
THis is a list of things that still need to be done
Project is divided into two major phases
PHASE 1: Get core functionality up and running
PHASE 2: Javascript and css on steroids
****/

TODO PHASE 1
/*
Implement proper logout
add csrf dtection to comment form
implement a timeline of sorts
add follows field to FotoUser model

#after completion of phase1: Push to repo branch master
*/

TODO PHASE 2
/*
#before phase 2 starts: create new branch and push code
Javascript loading where possible
only load new page when absolutely necessary
change css framework, check out bootstrap flat, Gumby, remember mobile first
***more detailed when this phase begins***
*/

PHASE 1 ends today
PHASE 2 ends next week
