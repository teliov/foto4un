"""
module containing all the forms for the photo sharing application
"""
from django_countries.countries import OFFICIAL_COUNTRIES

__author__ = 'teliov'

from django import forms

SEX_CHOICES = [
    ('M', 'MALE'),
    ('F', 'FEMALE'),
    ('O', 'OTHER'),
    ('N', 'NONE'),
]


class SignUpForm(forms.Form):
    """
    form for signing up
    """
    # TODO complete this form implementation
    username = forms.CharField(label='username', max_length=20, min_length=3,
                               widget=forms.TextInput(attrs={'class': 'special',
                                                             }))
    email = forms.EmailField(label='email address')
    password = forms.CharField(widget=forms.PasswordInput)
    password_repeat = forms.CharField(widget=forms.PasswordInput)


class LoginForm(forms.Form):
    """
    form for login in
    """
    # TODO complete this form implementation
    username = forms.CharField(label='username', max_length=20, min_length=3)
    password = forms.CharField(widget=forms.PasswordInput)


class UploadPicForm(forms.Form):
    """
    form for uploading a new picture
    """
    # TODO complete this form implementation
    #pic_name = forms.CharField(max_length=30, widget=forms.TextInput(attrs={
    #    'class': "form-control",
    #}))
    picture = forms.ImageField()
    pic_caption = forms.CharField(max_length=50, widget=forms.TextInput(attrs={
        'class': "form-control",
    }))


class EditProfileForm(forms.Form):
    """
    form for editing profile picture
    """
    # TODO complete this form implementation
    user_pic = forms.ImageField(widget=forms.FileInput)
    user_bio = forms.CharField(widget=forms.Textarea(attrs={
        'class': "form-control",
        'rows': '3',
    }), label='bio')
    user_sex = forms.ChoiceField(choices=SEX_CHOICES, label='sex', widget=forms.Select(attrs={
        'class': 'form-control',
    }))
    user_country = forms.ChoiceField(choices=OFFICIAL_COUNTRIES)
    user_dob = forms.DateField(widget=forms.DateInput)


class CommentForm(forms.Form):
    """
    form for commenting on a picture
    """
    # TODO complete this form implementation
    pass


class ChangePasswordForm(forms.Form):
    """
    form for changing password of a user
    """
    # TODO complete this form implementation
    pass