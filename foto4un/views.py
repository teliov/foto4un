"""
Views for Foto User
"""

import json
import pdb
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.core.context_processors import csrf
from django.core.exceptions import ObjectDoesNotExist
from django.http.response import HttpResponse, HttpResponseForbidden
from django.template.context import RequestContext
from django.utils.decorators import method_decorator
from django.views.generic import View
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render_to_response, redirect, get_object_or_404
from foto4un.forms import *
from foto4un.models import FotoUser, FotoPicture, Comments, ActivityItem as Item

__author__ = 'teliov'


class SignInView(View):
    """
    view specifically for signing in
    """
    # TODO complete this view
    def get(self, request):
        login_form = LoginForm()
        context = {'loginForm': login_form}
        context.update(csrf(request))
        return render_to_response('foto4un/signin.html', context)

    def post(self, request):
        errors = {}
        errors.update(csrf(request))
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            #pdb.set_trace()
            current_user = authenticate(username=username, password=password)
            if current_user is not None:
                if current_user.is_active:
                    login(request, current_user)
                    return redirect(current_user)
            else:
                errors = {'error': "Invalid Login"}
                return render_to_response('foto4un/signin.html', errors)
        else:
            return render_to_response('foto4un/signin.html', form.errors)


class SignUpView(View):
    """
    view specifically for signing up
    """
    # TODO complete this view
    def get(self, request):
        register_form = SignUpForm()
        return render_to_response('foto4un/signup.html', {'registerForm': register_form})

    def post(self, request):
        # TODO : change this method of validation, and error display messaging
        errors = {'exists': False}
        form = SignUpForm(request.POST)
        #pdb.set_trace()
        if form.is_valid():
            username = form.cleaned_data["username"]
            email = form.cleaned_data["email"]
            password = form.cleaned_data["password"]
            password_repeat = form.cleaned_data["password_repeat"]

            #pdb.set_trace()
            if password != password_repeat:
                errors['exists'] = True
                return redirect('/register/', error_data=errors)

            #check that both the username and the email address are not taken
            try:
                User.objects.get(username=username)
                User.objects.get(email=email)
            except ObjectDoesNotExist:
                user = User.objects.create_user(username, email, password)
                new_user = FotoUser(foto_user=user)
                new_user.user_profile_picture = '../../static/img/male-placeholder.jpg'
                new_user.save()
                new_user = authenticate(username=username, password=password)
                login(request, new_user)
                return redirect(new_user)

            errors['exists'] = True
            return redirect('/register/', error_data=errors)

        else:
            return redirect('/register/')


class FrontPageView(View):
    """
    View which handles the page shown to a non-logged in user
    """
    # TODO complete this view

    def get(self, request):
        """
        handles get requests
        """
        register_form = SignUpForm()
        login_form = LoginForm()
        context = {
            'registerForm': register_form,
            'loginForm': login_form,
        }
        context.update(csrf(request))
        return render_to_response('foto4un/front.html', context)


class FeedsListView(ListView):
    """
    displays the images from yourself and from your followers
    """
    # TODO complete this view

    @method_decorator(login_required)
    def get(self, request):
        """
        handles get requests
        """
        context = RequestContext(request)
        context.update(csrf(request))
        user = FotoUser.objects.get(foto_user=request.user)
        followers_list = user.followers.all()
        activity_items = Item.objects.filter(user__in=followers_list)[:10]
        context.update({'items':activity_items})
        return render_to_response('foto4un/feeds.html', context)


class PhotoListView(ListView):
    """
    gets a list of all photos uploaded by a user
    """
    @method_decorator(login_required)
    def get(self, request, username):
        context = RequestContext(request)
        user = get_object_or_404(FotoUser, foto_user__username=username)
        pics_list = user.user_pics.all()

        paginator = Paginator(pics_list, 10)

        page = request.GET.get('page')
        try:
            pics = paginator.page(page)
        except PageNotAnInteger:
            pics = paginator.page(1)

        except EmptyPage:
            pics = paginator.page(paginator.num_pages)

        context.update({'pictures': pics, 'user': user})
        return render_to_response('foto4un/pics_list.html', context)


class PictureDetailView(DetailView):
    """
    handles the viewing of a particular picture
    """
    # TODO complete this view

    @method_decorator(login_required)
    def get(self, request, photo_name):
        """
        handles get requests
        """
        picture = get_object_or_404(FotoPicture, pic_name=photo_name)
        #pdb.set_trace()
        pic_comments = Comments.objects.filter(picture=picture)
        context = RequestContext(request)
        context.update({'pic': picture, 'comments': pic_comments})
        return render_to_response('foto4un/picture_detail.html', context)


class UploadPhotoView(View):
    """
    handles photo uploads from user
    """
    @method_decorator(login_required)
    def get(self, request):
        form = UploadPicForm()
        context = {'form': form}
        context.update(csrf(request))
        return render_to_response('foto4un/upload.html', context, context_instance=RequestContext(request))

    @method_decorator(login_required)
    def post(self, request):
        #pdb.set_trace()
        form = UploadPicForm(request.POST, request.FILES)
        if form.is_valid():
            picture = form.cleaned_data["picture"]
            pic_caption = form.cleaned_data["pic_caption"]

            user = FotoUser.objects.get(foto_user=request.user)
            foto_pic = FotoPicture(picture=picture, pic_caption=pic_caption, pic_owner=user)
            foto_pic.save()
            name = foto_pic.picture.name.split('.')[0].split('/')[1]
            foto_pic.pic_name = name
            foto_pic.save()
            context = RequestContext(request)
            context.update({'success': True, 'name': name})

            return render_to_response('foto4un/success.html', context)

        else:
            form = UploadPicForm(request.POST)
            context = RequestContext(request)
            context.update(csrf(request))
            context.update({'form': form})
            return render_to_response('foto4un/upload.html', context)


class ProfileView(View):
    """
    handles the view for the profile of a user
    """
    # TODO complete this view

    @method_decorator(login_required)
    def get(self, request,  username):
        """
        handles get requests
        """
        context = RequestContext(request)
        try:
            foto_user = FotoUser.objects.get(foto_user__username=username)
            context.update({'user': foto_user})
            #pdb.set_trace()
            return render_to_response('foto4un/profile.html', context)
        except FotoUser.DoesNotExist:
            return HttpResponse('user does not exist')


class EditProfileView(View):
    """
    handles page view for editing profile
    """
    # TODO complete this view

    @method_decorator(login_required)
    def get(self, request, username):
        """
        handles get request
        """
        #pdb.set_trace()
        form = EditProfileForm()
        context = RequestContext(request)
        context.update({'form': form})
        context.update(csrf(request))
        return render_to_response('foto4un/edit_profile.html', context)

    @method_decorator(login_required)
    def post(self, request, username):
        context = {}
        context.update(csrf(request))
        form = EditProfileForm(request.POST)
        if form.is_valid:
            user_bio = form.cleaned_data['user_bio']
            user_sex = form.cleaned_data['user_sex']
            user_country = form.cleaned_data['user_country']
            user_dob = form.cleaned_data['user_dob']

            request.user(user_dob=user_dob, user_sex=user_sex, user_country=user_country, user_bio=user_bio)
            request.user.save()
            return render_to_response('foto4un/edit_profile.html', context)


class SearchView(View):
    """
    handles search for users
    """
    # TODO complete this view

    @method_decorator(login_required)
    def get(self, request):
        """
        handles get requests
        """
        return HttpResponse('Search Page')


class LogoutView(View):
    """
    logs out a user
    """
    # TODO complete this view
    pass


#functions to handle ajax requests
def validate_credentials(request):
    """
    validates the credentials i.e. username and password
    """
    valid = {'valid': False}
    #pdb.set_trace()
    if request.method == 'POST':
        name = request.POST.get("field_name")
        value = request.POST.get("field_value")
        validate_query = None
        if name == "username":
            validate_query = User.objects.filter(username=value)
        elif name == "email":
            validate_query = User.objects.filter(email=value)
        else:
            pass
        if validate_query is not None and len(validate_query) == 0:
            valid['valid'] = True
            data = json.dumps(valid)
        else:
            data = json.dumps(valid)

        return HttpResponse(data, content_type="application/json")


@login_required
def follow_user(request, username):
    """
    adds a user to followers
    """
    current_user = FotoUser.objects.get(foto_user=request.user)
    try:
        #check that the user with the username exists
        user = FotoUser.objects.get(foto_user__username=username)
        #check that the user is not already a follower
        try:
            current_user.followers.get(foto_user__username=user.foto_user.username)
            return HttpResponse('already following you')
        except FotoUser.DoesNotExist:
            current_user.followers.add(user)
            current_user.save()
            return HttpResponse('success')
    except FotoUser.DoesNotExist:
        return HttpResponseForbidden()


@login_required
def logout_user(request):
    """
    logs out a user
    """
    logout(request)
    return redirect('/home/')
    #return HttpResponse("logged out")

@login_required
def get_comment(request):
    """
    comment handler
    """
    if request.method == 'POST':
        try:
            username = request.POST['username']
            pic_name = request.POST['pic_name']
            comment = request.POST['user_comment']
        except KeyError:
            return HttpResponse("failed")
        user = get_object_or_404(FotoUser, foto_user__username=username)
        picture = get_object_or_404(FotoPicture, pic_name=pic_name)
        new_comment = Comments(commenter=user, picture=picture, comment=comment)
        new_comment.save()
        return redirect('picture_detail', pic_name)