from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from foto4un.views import *

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^home/', FrontPageView.as_view(), name='front'),
                       url(r'^comment/', get_comment, name='comment'),
                       url(r'^register/', SignUpView.as_view(), name='signup'),
                       url(r'^login/', SignInView.as_view(), name='signin'),
                       url(r'^logout/', logout_user, name='logout'),
                       url(r'^photos/view/(?P<photo_name>\w+)/', PictureDetailView.as_view(), name='picture_detail'),
                       url(r'^photos/(?P<username>\w+)/', PhotoListView.as_view(), name='picture_list'),
                       url(r'^users/(?P<username>\w+)/edit/', EditProfileView.as_view(), name='edit_profile'),
                       url(r'^users/(?P<username>\w+)/', ProfileView.as_view(), name='profile'),
                       url(r'^search/', SearchView.as_view(), name='search'),
                       url(r'^upload/', UploadPhotoView.as_view(), name='upload'),
                       url(r'^follow/(?P<username>\w+)/', follow_user, name='follow'),
                       url(r'^$', FeedsListView.as_view(), name='home'),
                       url(r'^check_credentials/', validate_credentials, name='validate_cred'),
                       ) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)