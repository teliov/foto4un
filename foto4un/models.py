"""
Contains the model files for the photo sharing application, fotofun
"""
import os
import pdb
import random
from django.contrib.contenttypes.generic import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db.models import signals

__author__ = 'teliov'

from django.db import models
from django.contrib.auth.models import User
from django_countries.fields import CountryField
from sorl.thumbnail import ImageField as SorlImageField

from datetime import datetime

SEX_CHOICES = [
    ('M', 'MALE'),
    ('F', 'FEMALE'),
    ('O', 'OTHER'),
    ('N', 'NONE'),
]


def update_filename(instance, filename):
    """
    renames the file that is being uploaded before it is saved on the server
    """
    #pdb.set_trace()
    if isinstance(instance, FotoPicture):
        path = "uploads"
    else:
        path = "profile_pics"

    re_list = ['-', ':', '.', " "]
    rand = str(random.randint(100000, 1000000))
    rand_2 = datetime.now().__str__()
    for char in re_list:
        rand_2 = rand_2.replace(char, "")
    ext = instance.picture.name.split('.')[1]
    name = "".join([rand, "_", rand_2, '.', ext])
    return os.path.join(path, name)


class FotoUser(models.Model):
    """
    Model for a user of the fotofun application
    """
    foto_user = models.OneToOneField(User, verbose_name="User", primary_key=True)
    user_bio = models.TextField(verbose_name="Bio", blank=True)
    user_sex = models.CharField(choices=SEX_CHOICES, verbose_name="Sex", blank=True, max_length=10)
    user_country = CountryField(verbose_name="Country", blank=True)
    user_dob = models.DateField(verbose_name="date of birth", null=True)
    user_profile_picture = SorlImageField(blank=True, verbose_name="profile_pic", upload_to="profile_pics")
    followers = models.ManyToManyField('self', verbose_name="followers", blank=True)

    def __str__(self):
        """
        returns the string representation for a user
        """
        return self.foto_user.username

    def __unicode__(self):
        """
        returns a unicode representation of the model
        """
        return self.foto_user.username

    def get_absolute_url(self):
        """
        returns the url of the particular user
        """
        return "/users/%s" % self.foto_user.username

    def calculate_age(self):
        """
        calculates the age of the user
        """
        now = datetime.now()
        if self.user_dob is not None:
            age = self.user_dob.year - now.year - 1

            if self.user_dob.month > now.month and self.user_dob.day > now.day:
                age += 1

            return age
        return None

    class Meta:
        """
        meta class for fotouser model
        """
        ordering = ['foto_user']
        verbose_name = "User"


class FotoPicture(models.Model):
    """
    Model for a single picture
    """
    picture = SorlImageField(upload_to=update_filename, verbose_name="Pictures")
    pic_name = models.CharField(max_length=50)
    pic_caption = models.CharField(max_length=20, blank=True)
    pic_owner = models.ForeignKey(FotoUser, related_name='user_pics')
    date_posted = models.DateTimeField(auto_now=True)
    pic_likes = models.IntegerField(verbose_name="likes", default=0)
    pic_dislikes = models.IntegerField(verbose_name="likes", default=0)

    class Meta:
        """
        Meta class for FotoPicture model
        """
        ordering = ['date_posted']
        verbose_name = "Picture"

    def __str__(self):
        """
        return string representation of the model
        """
        return self.picture.name

    def __unicode__(self):
        """
        returns a unicode representation of the model
        """
        return self.picture.name

    def get_absolute_url(self):
        """
        returns the absolute url of the image
        """
        return "/photos/%s" % self.pic_name


class Comments(models.Model):
    """
    Model for comments on a particular picture
    """
    commenter = models.ForeignKey(FotoUser, verbose_name="comment owner")
    picture = models.ForeignKey(FotoPicture, verbose_name="picture", related_name="pic_comments")
    comment = models.CharField(max_length=50, verbose_name="comment")
    date_posted = models.DateTimeField(auto_now=True, verbose_name="date posted")
    comment_likes = models.IntegerField(default=0, verbose_name="likes")
    comment_dislikes = models.IntegerField(default=0, verbose_name="dislikes")

    class Meta:
        """
        Meta class for Comment Model
        """
        verbose_name = 'Comment'
        ordering = ['-date_posted']


class ActivityItem(models.Model):
    """
    model to store recent activites for a user
    this model is created anytime a user or his followers comments, likes or uploads a picture
    or anytime a picture owned by the user is commented on
    """
    user = models.ForeignKey(FotoUser)
    time_created = models.DateTimeField(auto_now=True)
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        """
        meta class for activities model
        """
        get_latest_by = ['-time_created']

    def __unicode__(self):
        """
        return unicode representation of the model
        """
        return "%s_%s_activity" %(self.user.__str__(), self.content_object.__str__())


def create_activity_item(sender, instance, signal, *args, **kwargs):
    """
    creates an activity item after either a comment or a picture is created or liked
    """
    #pdb.set_trace()
    ctype = ContentType.objects.get_for_model(type(instance))

    if ctype.name == "Picture":
        time_created = instance.date_posted
        user = instance.pic_owner

    if ctype.name == "Comment":
        time_created = instance.date_posted
        user = instance.commenter
        instance = instance.picture
        ctype = ContentType.objects.get_for_model(instance)


    object_id = instance.id

    activity, isCreated = ActivityItem.objects.get_or_create(content_type=ctype, object_id=object_id)

    activity.time_created = time_created
    #activity.user = user
    activity.save()


signals.post_save.connect(create_activity_item, FotoPicture)
signals.post_save.connect(create_activity_item, Comments)